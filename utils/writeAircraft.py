import argparse
import time
import json
import os

parser = argparse.ArgumentParser()

parser.add_argument("source_path",
                    help="filepath to file with the aircraft.json record")
parser.add_argument("destination_folder",
                    help="path where aircraft.json will be written")
parser.add_argument("--sleep",
                    type=int,
                    default=1,
                    help="interval of writing aircraft.json file in seconds ")

args = parser.parse_args()

with open (args.source_path) as src:
    aircrafts=json.load(src)

for aircraft in aircrafts["aircrafts"]:
    print(aircraft)
    aircraft["now"]= float(time.time())

    with open(os.path.join(args.destination_folder,"aircraft.json"),'w') as dst:
        json.dump(aircraft,dst)
    time.sleep(args.sleep)