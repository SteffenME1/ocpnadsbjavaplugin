import argparse
import datetime
import time
import json
from os import path

parser = argparse.ArgumentParser()

parser.add_argument("source_path",
                    help="aircraft.json with path")
parser.add_argument("destination_folder",
                    help="path where the output will be saved")
parser.add_argument("-m","--minutes",
                    type=lambda m: datetime.timedelta(minutes=int(m)), default=datetime.timedelta(minutes=20),
                    help="record time in minutes")
parser.add_argument("--sleep",
                    type=int,
                    default=1,
                    help="delay between data points in seconds")

args = parser.parse_args()

aircrafts = {"aircrafts":[]}
stringAircraft = ""
end=datetime.datetime.now()+args.minutes

while datetime.datetime.now() <= end:
    with open(args.source_path,'r') as src:
        jsonAircraft = json.load(src)

    if stringAircraft != str(jsonAircraft):
        aircrafts["aircrafts"].append(jsonAircraft)
        stringAircraft=str(jsonAircraft)
    time.sleep(args.sleep)

with open(path.join(args.destination_folder,f"aircrafts{datetime.datetime.now()}.json"),'w') as dst:
    json.dump(aircrafts,dst)