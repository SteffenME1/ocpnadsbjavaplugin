KnownAssetsList = require('./knownAssetsList.js');

test('returns nullish value if the list is empty', () => {
  expect(
    new KnownAssetsList('').findByHexId('600bc8')
  ).toBeUndefined();
});

test("won't treat the headline as a valid entry", () => {
  expect(
    new KnownAssetsList('Hex;Callsign / Registration / Name;Operator and Comments').findByHexId('Hex')
  ).toBeUndefined();
});

test("returns a valid entry if the hex code matches", () => {
  expect(
    new KnownAssetsList('Hex;Callsign / Registration / Name;Operator and Comments\n600bc8;4K-ASG;ASG Business Aviation\n3C648D;D-AIDM;Lufthansa').findByHexId('600bc8')
  ).toEqual({
    hex: '600bc8',
    opsCallsign: '4K-ASG',
    operatorAndComments: 'ASG Business Aviation'
  });
});

test("ignores white space around fields separators", () => {
  expect(
    new KnownAssetsList(' Hex; Callsign / Registration / Name; Operator and Comments  \n 600bc8 ; 4K-ASG  ; ASG Business Aviation ').findByHexId('600bc8')
  ).toEqual({
    hex: '600bc8',
    opsCallsign: '4K-ASG',
    operatorAndComments: 'ASG Business Aviation'
  });
});

test("ignores trailing empty lines", () => {
  const parsedAssetsList = new KnownAssetsList('Hex;Callsign / Registration / Name;Operator and Comments\n600bc8;4K-ASG;ASG Business Aviation\n\n');

  // regular line is found...
  expect(parsedAssetsList.findByHexId('600bc8').hex).toEqual('600bc8');

  // but an empty one is not
  expect(parsedAssetsList.findByHexId('')).toBeUndefined();
});

test("also works well with windows line breaks", () => {
  const parsedAssetsList = new KnownAssetsList('Hex;Callsign / Registration / Name;Operator and Comments\r\n600bc8;4K-ASG;ASG Business Aviation');
  expect(parsedAssetsList.findByHexId('600bc8').hex).toEqual('600bc8');
});

test("hex codes are matched case insensitive (uppercase in CSV, lower case wanted)", () => {
  const parsedAssetsList = new KnownAssetsList('Hex;Callsign / Registration / Name;Operator and Comments\n600BC8;4K-ASG;ASG Business Aviation');
  expect(parsedAssetsList.findByHexId('600bc8').hex).toEqual('600BC8');
});