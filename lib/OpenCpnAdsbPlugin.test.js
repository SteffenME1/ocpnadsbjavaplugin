OpenCpnAdsbPlugin = require('./OpenCpnAdsbPlugin.js');


var mockConfig = {
  "aircraftFile": "./aircraft.json",
  "deleteAfterSeconds": 3600,
  "altitudeThreshold": 10000,
  "dialogueRefreshRate": 6,
  "waypointDistance": 2,
  "altitudeAlarmThreshold": 32000,
  "circleAlarmThreshold": 200,
  "distanceAlarmThreshold": 20,
  "ignoredFlightNumberPrefixes": []
};

function OpenCpnApiMock(initialRoutes = []) {
  var consoleHidden = false;
  var canvasRefreshed = false;
  var routes = initialRoutes;
  var alarmTriggered = false;
  var dialogueCallback = () => {
  };

  return {
    consoleHide: () => consoleHidden = true,
    getRouteGUIDs: () => routes.map(r => r.GUID),
    getAllRoutes: () => routes,
    getRoute: guid => routes.find(r => r.GUID = guid),
    addRoute: route => {
      routes = [...routes, route];
      canvasRefreshed = false;
      return route.GUID;
    },
    isConsoleHidden: () => consoleHidden,
    getNavigation: () => ({
      position: 'ABC'
    }),
    getVectorPP: (positionA, positionB) => {
      return { distance: 12345 };
    },
    deleteRoute: guid => {
      routes = routes.filter(r => r.GUID !== guid);
      canvasRefreshed = false;
    },
    refreshCanvas: () => canvasRefreshed = true,
    isCanvasRefreshed: () => canvasRefreshed,
    soundAlarm: () => alarmTriggered = true,
    isAlarmTriggered: () => alarmTriggered,
    onDialogue: (callback, dialogue) => dialogueCallback = callback,
    confirmDialogue: dialogueResults => dialogueCallback(dialogueResults)
  }
}

function RouteMock(name = undefined) {
  this.GUID = '';
  this.name = name;
  this.waypoints = [];
}


function KnownAssetsMock() {
  this.findByHexId = () => undefined;
}

var aircraftShouldBeIgnoredMock = () => false;

function WaypointMock(latitude, longitude) {
  this.position = { latitude, longitude }
}

function MockAircraftSource(initialData) {
  var dataToReturn = initialData;
  return {
    refetch: () => dataToReturn,
    setDataToReturn: newData => {
      dataToReturn = newData
    },
  };
}


describe('initialization on startup', () => {
  it('hides console', () => {
    var openCpnApiMock = new OpenCpnApiMock();

    new OpenCpnAdsbPlugin(
      mockConfig,
      new KnownAssetsMock,
      aircraftShouldBeIgnoredMock,
      RouteMock,
      WaypointMock,
      openCpnApiMock,
      { refetch: () => ({ "now": 12345, "aircraft": [] }) }
    );

    expect(openCpnApiMock.isConsoleHidden()).toBeTruthy();
  });

  it('deletes routes with only outdated waypoints', () => {
    var openCpnApiMock = new OpenCpnApiMock(
      [{
        GUID: 'djfknvdjf',
        name: 'Flight: ABCDE',
        waypoints: [
          {
            description: 'Altitude: 1234ft',
            creationDateTime: 123
          }
        ]
      }]
    );

    new OpenCpnAdsbPlugin(
      { ...mockConfig, deleteAfterSeconds: 1 },
      new KnownAssetsMock(),
      aircraftShouldBeIgnoredMock,
      RouteMock,
      WaypointMock,
      openCpnApiMock,
      { refetch: () => ({ "now": 12345, "aircraft": [] }) }
    );

    expect(openCpnApiMock.getAllRoutes()).toHaveLength(0);
    expect(openCpnApiMock.isCanvasRefreshed()).toBeTruthy();
  });

  it('keeps routes with at least one non-outdated waypoint', () => {
    var openCpnApiMock = new OpenCpnApiMock(
      [{
        GUID: 'djfknvdjf',
        name: 'Flight: ABCDE',
        waypoints: [
          {
            description: 'Altitude: 1234ft',
            creationDateTime: 123
          },
          {
            description: 'Altitude: 1234ft',
            creationDateTime: 12345678918789
          }
        ]
      }]
    );

    new OpenCpnAdsbPlugin(
      { ...mockConfig, deleteAfterSeconds: 1 },
      new KnownAssetsMock(),
      aircraftShouldBeIgnoredMock,
      RouteMock,
      WaypointMock,
      openCpnApiMock,
      { refetch: () => ({ "now": 12345, "aircraft": [] }) }
    );

    expect(openCpnApiMock.getAllRoutes()).toHaveLength(1);
  });

  it("won't delete routes that are not created by the plugin ", () => {
    // Routes created by us have a "Flight: " name prefix
    var openCpnApiMock = new OpenCpnApiMock(
      [{
        GUID: 'vdmkfmdk',
        name: 'Port approach',
        waypoints: [
          {
            description: 'Foo',
            creationDateTime: 123
          }
        ]
      }]
    );

    new OpenCpnAdsbPlugin(
      { ...mockConfig, deleteAfterSeconds: 1 },
      new KnownAssetsMock(),
      aircraftShouldBeIgnoredMock,
      RouteMock,
      WaypointMock,
      openCpnApiMock,
      { refetch: () => ({ "now": 12345, "aircraft": [] }) }
    );

    expect(openCpnApiMock.getAllRoutes()).toHaveLength(1);
    expect(openCpnApiMock.getAllRoutes()[0].name).toBe('Port approach');
    expect(openCpnApiMock.getAllRoutes()[0].waypoints).toHaveLength(1);
  });
});

describe('regular operations', () => {
  it('adds a flight as a route to OpenCPN if it three or more waypoints', () => {
    var openCpnApiMock = new OpenCpnApiMock([]);
    var mockAircraftSource = new MockAircraftSource(({ "now": 12344, "aircraft": [] }));

    var openCpnPlugin = new OpenCpnAdsbPlugin(
      {
        ...mockConfig,
        altitudeThreshold: 40000,
        altitudeAlarmThreshold: 35000,
        waypointDistance: 1,
        deleteAfterSeconds: 9999999999999999
      },
      new KnownAssetsMock(),
      aircraftShouldBeIgnoredMock,
      RouteMock,
      WaypointMock,
      openCpnApiMock,
      mockAircraftSource
    );

    mockAircraftSource.setDataToReturn(({
      "now": 12345,
      "aircraft": [{
        "hex": "3c5ee2",
        "flight": "EWG3W   ",
        "alt_baro": 25123,
        "true_heading": 275.30,
        "lat": 51.209839,
        "lon": 5.413216,
        "seen": 3.3
      }]
    }))
    openCpnPlugin.loopFlights();

    mockAircraftSource.setDataToReturn(({
      "now": 12346,
      "aircraft": [{
        "hex": "3c5ee2",
        "flight": "EWG3W   ",
        "alt_baro": 25124,
        "true_heading": 274.30,
        "lat": 51.219839,
        "lon": 5.423216,
        "seen": 3.3
      }]
    }))
    openCpnPlugin.loopFlights();

    mockAircraftSource.setDataToReturn(({
      "now": 12347,
      "aircraft": [{
        "hex": "3c5ee2",
        "flight": "EWG3W   ",
        "alt_baro": 25123,
        "true_heading": 275.30,
        "lat": 51.229839,
        "lon": 5.433216,
        "seen": 3.3
      }]
    }))
    openCpnPlugin.loopFlights();

    expect(openCpnApiMock.isAlarmTriggered()).toBeTruthy();
    openCpnApiMock.confirmDialogue([{}, {}, { value: [] }, { label: 'show' }]);

    expect(openCpnApiMock.getAllRoutes()).toHaveLength(1);
    expect(openCpnApiMock.getAllRoutes()[0].waypoints).toHaveLength(3);
    expect(openCpnApiMock.isCanvasRefreshed()).toBeTruthy();
  })
})