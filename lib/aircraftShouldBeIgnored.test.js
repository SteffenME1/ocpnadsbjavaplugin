aircraftShouldBeIgnored = require('./aircraftShouldBeIgnored.js');

test('returns false if no ignore rule matches', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft(),
      createStubConfig()
    )
  ).toBe(false);
});

test('returns true if the aircraft is above the configured altitude threshold', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft({ altitude: 300 }),
      createStubConfig({ altitudeThreshold: 200 })
    )
  ).toBe(true);
})

test('does not throw if the aircraft is missing a flight number', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft({ flight: null }),
      createStubConfig({ ignoredFlightNumberPrefixes: ['KLM'] })
    )
  ).toBe(false);
})

test('returns false if the flight number does not start with an ignored Prefix', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft({ flight: 'LWA811' }),
      createStubConfig({ ignoredFlightNumberPrefixes: ['KLM'] })
    )
  ).toBe(false);
})

test('returns true if the flight number starts with an ignored Prefix', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft({ flight: 'KLM4711' }),
      createStubConfig({ ignoredFlightNumberPrefixes: ['FBR1', 'KLM'] })
    )
  ).toBe(true);
})

test('does not throw if ignoredFlightNumberPrefixes is missing in config', () => {
  expect(
    aircraftShouldBeIgnored(
      createStubAircraft({ flight: 'KLM4711' }),
      createStubConfig({ ignoredFlightNumberPrefixes: null })
    )
  ).toBe(false);
})

function createStubAircraft({ altitude = undefined, flight = undefined } = {}) {
  const aircraft = {
    "hex": "3c61f8",
    "type": "adsb_icao",
    "alt_baro": altitude ?? 1000,
    "alt_geom": 39825,
    "gs": 449.0,
    "ias": 234,
    "mach": 0.776,
    "track": 200.33,
    "mag_heading": 212.87,
    "true_heading": 215.16,
    "baro_rate": 64,
    "squawk": "0752",
    "lat": 51.765015,
    "lon": 5.035004,
    "nic": 8,
    "rc": 186,
    "seen_pos": 58.675,
    "version": 0,
    "nac_p": 8,
    "nac_v": 2,
    "sil": 2,
    "sil_type": "unknown",
    "alert": 0,
    "spi": 0,
    "mlat": [],
    "tisb": [],
    "messages": 164,
    "seen": 19.3,
    "rssi": -13.6
  };

  if (flight !== null) {
    aircraft.flight = flight ?? 'ABD123';
  }
  return aircraft
}

function createStubConfig({ altitudeThreshold, ignoredFlightNumberPrefixes } = {}) {
  const config = {
    "deleteAfterSeconds": 3600,
    "altitudeThreshold": altitudeThreshold ?? 40000,
    "dialogueRefreshRate": 6,
    "waypointDistance": 2,
    "altitudeAlarmThreshold": 32000,
    "circleAlarmThreshold": 200,
    "distanceAlarmThreshold": 20
  };

  if (ignoredFlightNumberPrefixes !== null) {
    config.ignoredFlightNumberPrefixes = ignoredFlightNumberPrefixes ?? [];
  }
  return config;
}