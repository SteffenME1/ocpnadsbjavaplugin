function aircraftShouldBeIgnored(aircraft, config) {
  if (aircraft.alt_baro > config.altitudeThreshold) {
    return true;
  }

  if ((config.ignoredFlightNumberPrefixes || []).some(function (ignoredPrefix) { return (aircraft.flight || '').startsWith(ignoredPrefix)})) {
    return true;
  }

  return false;
}

module.exports = aircraftShouldBeIgnored;